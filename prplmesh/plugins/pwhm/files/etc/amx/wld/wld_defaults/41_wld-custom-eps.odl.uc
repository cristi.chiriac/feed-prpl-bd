%populate {
    object WiFi {
{% for ( let Radio in BD.Radios ) : if (Radio.OperatingFrequency == "5GHz") : %}
{% RadioIndex = BDfn.getRadioIndex(Radio.OperatingFrequency);
if (RadioIndex >= 0) : %}
        object SSID {
            instance add (5, "ep5g0") {
                parameter LowerLayers = "Device.WiFi.Radio.{{RadioIndex + 1}}.";
            }
        }
        object EndPoint {
            instance add ("ep5g0") {
                parameter SSIDReference = "Device.WiFi.SSID.5.";
                parameter Enable = 0;
                parameter BridgeInterface = "{{BD.Bridges.Lan.Name}}";
                object WPS {
                    parameter Enable = 1;
                    parameter ConfigMethodsEnabled = "PhysicalPushButton,VirtualPushButton,VirtualDisplay,PIN";
                }
            }
        }
{% endif %}
{% endif; endfor; %}
    }
}
