%populate {
    object WiFi {
        object AccessPoint {
{% for ( let Itf in BD.Interfaces ) : if ( Itf.Type == "wireless" ) : %}
            object '{{Itf.Alias}}' {
{% if (BDfn.isInterfaceGuest(Itf.Name)) : %}
                parameter BridgeInterface = "{{BD.Bridges.Guest.Name}}";
{% else %}
                parameter BridgeInterface = "{{BD.Bridges.Lan.Name}}";
{% endif %}
            }
{% endif; endfor; %}
        }
    }
}
